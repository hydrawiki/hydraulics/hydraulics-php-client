<?php

declare(strict_types=1);

namespace Hydrawiki\Hydraulics\Client\V1\Resources;

use Hydrawiki\Hydraulics\Client\V1\Resource;

class Environment extends Resource
{
    /**
     * Resource type as per the API.
     *
     * @var string
     */
    protected $type = 'environments';

    /**
     * Attributes provided by the API and default values.
     *
     * @var array
     */
    protected $attributes = [
        'created-at'        => null,
        'database-hostname' => null,
        'database-password' => null,
        'database-username' => null,
        'name'              => null,
        'updated-at'        => null,
        'wiki-limit'        => null,
    ];

    /**
     * Relationships to other Resources.
     *
     * @var array
     */
    protected $relationships = [
        'wikis' => [Wiki::class, self::RELATIONSHIP_MANY],
    ];
}
