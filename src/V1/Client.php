<?php

declare(strict_types=1);

namespace Hydrawiki\Hydraulics\Client\V1;

use Hydrawiki\Hydraulics\Client\V1\Exceptions\ApiRequestUnsuccessful;
use Hydrawiki\Hydraulics\Client\V1\Exceptions\ClientResourceCall;
use Tightenco\Collect\Support\Collection;

class Client
{
    /**
     * API Client responsible for making requests.
     *
     * @var \Hydrawiki\Hydraulics\Client\V1\Api
     */
    protected $api;

    /**
     * Hydrator responsible for hydrating Resources.
     *
     * @var \Hydrawiki\Hydraulics\Client\V1\Hydrator
     */
    protected $hydrator;

    /**
     * Resources used to build the request path.
     *
     * @var \Tightenco\Collect\Support\Collection
     */
    protected $resources;

    /**
     * Parameters to include as the request's query string.
     *
     * @var \Tightenco\Collect\Support\Collection
     */
    protected $parameters;

    /**
     * Constructs a new Client.
     *
     * @param \Hydrawiki\Hydraulics\Client\V1\Api      $api
     * @param \Hydrawiki\Hydraulics\Client\V1\Hydrator $hydrator
     */
    public function __construct(Api $api, Hydrator $hydrator)
    {
        $this->api = $api;
        $this->hydrator = $hydrator;
        $this->newCollectionState();
    }

    /**
     * Add a resource to the request path, e.g: `wikis()`.
     *
     * @param string $resource
     * @param array  $parameters
     *
     * @throws \Hydrawiki\Hydraulics\Client\V1\Exceptions\ClientResourceCall
     *
     * @return \Hydrawiki\Hydraulics\Client\V1\Client
     */
    public function __call(string $resource, array $parameters)
    {
        if (count($parameters) > 1) {
            throw ClientResourceCall::parameters($parameters);
        }

        $object = reset($parameters);

        if ($object && !$object instanceof Resource) {
            throw ClientResourceCall::type($object);
        }

        $this->resources->push(new Collection([
              'type'   => $resource,
              'object' => $object,
          ]));

        return $this;
    }

    /**
     * Retrieve an index of a Resource with all primary resources hydrated.
     *
     * @return \Tightenco\Collect\Support\Collection
     */
    public function all(): Collection
    {
        $response = $this->api->get($this->compilePath());

        if (!$response->isSuccessfulIndex()) {
            throw ApiRequestUnsuccessful::index($response);
        }

        return $this->hydrator->hydrate($response->document());
    }

    /**
     * Find a single Resource by its ID.
     *
     * @param string $id
     *
     * @return \Hydrawiki\Hydraulics\Client\V1\Resource
     */
    public function find(string $id): Resource
    {
        $response = $this->api->get($this->compilePath($id));

        if (!$response->isSuccessfulRead()) {
            throw ApiRequestUnsuccessful::read($response);
        }

        return $this->hydrator->hydrate($response->document());
    }

    /**
     * Add one or more relations to be included.
     *
     * @param string ...$relations
     *
     * @return \Hydrawiki\Hydraulics\Client\V1\Client
     */
    public function include(string ...$relations): self
    {
        $this->parameters->put(
            'includes',
            $this->parameters->get('includes', []) + $relations
        );

        return $this;
    }

    /**
     * Compile the path for the request, resetting the client state.
     *
     * @param string|null $id
     *
     * @return string
     */
    protected function compilePath(?string $id = null): string
    {
        $path = $this->resources
            ->map(function ($resource) {
                return [
                    $resource->get('type'),
                    $resource->get('object') ? $resource->get('object')->id() : null,
                ];
            })
            ->flatten()
            ->merge($id)
            ->filter()
            ->implode('/');

        $query = $this->queryString();

        $this->newCollectionState();

        return $path.($query ? "?{$query}" : '');
    }

    /**
     * Build the query string for the request.
     *
     * @return string
     */
    protected function queryString(): string
    {
        return http_build_query(array_merge(
            $this->includes()
        ));
    }

    /**
     * Get the list of relations to include in the request.
     *
     * @return array
     */
    protected function includes(): array
    {
        $includes = $this->parameters->get('includes', []);

        return $includes ? ['include' => implode(',', $includes)] : [];
    }

    /**
     * Reset the state of the collections used by the client.
     *
     * @return void
     */
    protected function newCollectionState(): void
    {
        $this->resources = new Collection();
        $this->parameters = new Collection();
    }
}
