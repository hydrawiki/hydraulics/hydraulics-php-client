<?php

declare(strict_types=1);

namespace Hydrawiki\Hydraulics\Client\V1;

use Tightenco\Collect\Support\Collection;
use WoohooLabs\Yang\JsonApi\Schema\Document as YangDocument;

class Document
{
    /**
     * Yang Document.
     *
     * @var \WoohooLabs\Yang\JsonApi\Schema\Document
     */
    protected $document;

    /**
     * Constructs a new Document wrapper around a Yang Document.
     *
     * @param \WoohooLabs\Yang\JsonApi\Schema\Document $document
     */
    public function __construct(YangDocument $document)
    {
        $this->document = $document;
    }

    /**
     * Get all (primary, included) resources that are in the Document.
     *
     * @return \Tightenco\Collect\Support\Collection
     */
    public function allResources(): Collection
    {
        return $this->primaryResources()->merge($this->includedResources());
    }

    /**
     * Get primary resources that are in the Document.
     *
     * @return \Tightenco\Collect\Support\Collection
     */
    public function primaryResources(): Collection
    {
        $type = $this->isOne() ? 'primaryResource' : 'primaryResources';

        $resources = (new Collection())->wrap($this->document->{$type}());

        return $this->wrapAsResourceObjects($resources);
    }

    /**
     * Get resources that were included in the document.
     *
     * @return \Tightenco\Collect\Support\Collection
     */
    public function includedResources(): Collection
    {
        return $this->wrapAsResourceObjects(collect($this->document->includedResources()));
    }

    /**
     * Does the Document provide a single primary resource?
     *
     * @return bool
     */
    public function isOne(): bool
    {
        return $this->document->isSingleResourceDocument();
    }

    /**
     * Does the document provide many primary resources?
     *
     * @return bool
     */
    public function isMany(): bool
    {
        return $this->document->isResourceCollectionDocument();
    }

    /**
     * Wrap each Yang ResourceObject in our own ResourceObject class.
     *
     * @param \Tightenco\Collect\Support\Collection $objects
     *
     * @return \Tightenco\Collect\Support\Collection
     */
    protected function wrapAsResourceObjects(Collection $objects): Collection
    {
        return $objects->map(function ($object) {
            return new ResourceObject($object);
        });
    }
}
