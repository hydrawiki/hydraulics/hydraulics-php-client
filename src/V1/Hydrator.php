<?php

declare(strict_types=1);

namespace Hydrawiki\Hydraulics\Client\V1;

use Tightenco\Collect\Support\Collection;

class Hydrator
{
    /**
     * Resource Factory.
     *
     * @var \Hydrawiki\Hydraulics\Client\V1\ResourceFactory
     */
    protected $resourceFactory;

    /**
     * Constructs a new Resource Hydrator from a Document.
     *
     * @param \Hydrawiki\Hydraulics\Client\V1\ResourceFactory $resourceFactory
     */
    public function __construct(ResourceFactory $resourceFactory)
    {
        $this->resourceFactory = $resourceFactory;
    }

    /**
     * Hydrates a Document by turning Resource Objects into Resources with their
     * attributes and relations. Returns either a single primary resource or a
     * Collection of primary resources depending on the Document type.
     *
     * @param \Hydrawiki\Hydraulics\Client\V1\Document $document
     *
     * @return \Tightenco\Collect\Support\Collection|\Hydrawiki\Hydraulics\Client\V1\Resource
     */
    public function hydrate(Document $document)
    {
        $resources = $document->allResources()->mapWithKeys(function ($object) {
            return [$object->key() => $this->resourceFactory->make($object->type())];
        });

        $document->allResources()->each(function ($object) use ($resources) {
            $resources
                ->get($object->key())
                ->setId($object->id())
                ->setAttributes($object->attributes())
                ->setMeta($object->meta())
                ->setRelations($this->hydrateRelations($object, $resources));
        });

        $primary = $document->primaryResources()->map(function ($primary) use ($resources) {
            return $resources->get($primary->key());
        });

        return $document->isOne() ? $primary->first() : $primary;
    }

    /**
     * Hydrates relations on a Resource, turning 'relationship' => [[type, id]]
     * into 'relationship' => [Resource, Resource, Resource].
     *
     * @param \Hydrawiki\Hydraulics\Client\V1\ResourceObject $object
     * @param \Tightenco\Collect\Support\Collection          $resources
     *
     * @return array
     */
    protected function hydrateRelations(ResourceObject $object, Collection $resources): array
    {
        return $object->relations()->map(function ($relations, $relationship) {
            return collect($relations)->map(function ($relation) use ($relationship) {
                return $relation + [
                    'relationship' => $relationship,
                    'key'          => "{$relation['type']}.{$relation['id']}",
                ];
            });
        })
        ->flatten(1)
        ->mapToGroups(function ($relation) use ($resources) {
            return [$relation['relationship'] => $resources->get($relation['key'])];
        })
        ->toArray();
    }
}
