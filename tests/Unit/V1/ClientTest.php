<?php

declare(strict_types=1);

namespace Hydrawiki\Hydraulics\Client\Tests\Unit\V1;

use Hydrawiki\Hydraulics\Client\V1\Api;
use Hydrawiki\Hydraulics\Client\V1\Client;
use Hydrawiki\Hydraulics\Client\V1\Document;
use Hydrawiki\Hydraulics\Client\V1\Exceptions\ApiRequestUnsuccessful;
use Hydrawiki\Hydraulics\Client\V1\Exceptions\ClientResourceCall;
use Hydrawiki\Hydraulics\Client\V1\Hydrator;
use Hydrawiki\Hydraulics\Client\V1\JsonApiResponse;
use Hydrawiki\Hydraulics\Client\V1\Resource;
use PHPUnit\Framework\TestCase;
use Tightenco\Collect\Support\Collection;

class ClientTest extends TestCase
{
    /**
     * Tests that when a resource call is made with multiple parameters an
     * exception is thrown.
     */
    public function testTooManyResourceParametersThrowsException(): void
    {
        $client = new Client($this->createMock(Api::class), $this->createMock(Hydrator::class));

        $this->expectException(ClientResourceCall::class);

        $client->resources(1, 2);
    }

    /**
     * Tests that when a resource call is made with a non-Resource object that
     * an exception is thrown.
     */
    public function testNonResourceObjectThrowsException(): void
    {
        $client = new Client($this->createMock(Api::class), $this->createMock(Hydrator::class));

        $this->expectException(ClientResourceCall::class);

        $client->resources($client);
    }

    /**
     * Tests that the client retrieves an index of Resources.
     */
    public function testClientIndexesResources(): void
    {
        $document = $this->createMock(Document::class);

        $response = $this->createMock(JsonApiResponse::class);
        $response->method('isSuccessfulIndex')->willReturn(true);
        $response->method('document')->willReturn($document);

        $hydrator = $this->createMock(Hydrator::class);
        $hydrator->expects($this->once())
            ->method('hydrate')
            ->with($document)
            ->willReturn(new Collection());

        $api = $this->createMock(Api::class);
        $api->expects($this->once())
            ->method('get')
            ->with('resources')
            ->willReturn($response);

        $client = new Client($api, $hydrator);
        $client->resources()->all();
    }

    /**
     * Tests that when an Index is unsuccessful that an Exception is thrown.
     */
    public function testUnsuccessfulIndexThrowsException(): void
    {
        $document = $this->createMock(Document::class);

        $response = $this->createMock(JsonApiResponse::class);
        $response->method('isSuccessfulIndex')->willReturn(false);

        $api = $this->createMock(Api::class);
        $api->expects($this->once())
            ->method('get')
            ->with('resources')
            ->willReturn($response);

        $client = new Client($api, $this->createMock(Hydrator::class));

        $this->expectException(ApiRequestUnsuccessful::class);

        $client->resources()->all();
    }

    /**
     * Tests that nested resources can be accessed through a fluent interface.
     */
    public function testNestedRelationshipRequests(): void
    {
        $document = $this->createMock(Document::class);

        $response = $this->createMock(JsonApiResponse::class);
        $response->method('isSuccessfulIndex')->willReturn(true);
        $response->method('document')->willReturn($document);

        $hydrator = $this->createMock(Hydrator::class);
        $hydrator->expects($this->once())
            ->method('hydrate')
            ->with($document)
            ->willReturn(new Collection());

        $api = $this->createMock(Api::class);
        $api->expects($this->once())
            ->method('get')
            ->with('resources/1/children')
            ->willReturn($response);

        $resource = $this->createMock(Resource::class);
        $resource->method('id')->willReturn('1');

        $client = new Client($api, $hydrator);
        $client->resources($resource)->children()->all();
    }

    /**
     * Tests that a single resource is retrieved by its ID.
     */
    public function testClientRequestsSingleResource(): void
    {
        $document = $this->createMock(Document::class);

        $response = $this->createMock(JsonApiResponse::class);
        $response->method('isSuccessfulRead')->willReturn(true);
        $response->method('document')->willReturn($document);

        $hydrator = $this->createMock(Hydrator::class);
        $hydrator->expects($this->once())
            ->method('hydrate')
            ->with($document)
            ->willReturn(
                new class() extends Resource {
                }
            );

        $api = $this->createMock(Api::class);
        $api->expects($this->once())
            ->method('get')
            ->with('resources/1')
            ->willReturn($response);

        $client = new Client($api, $hydrator);
        $client->resources()->find('1');
    }

    /**
     * Tests that the client's state is reset between requests.
     */
    public function testSubsequentRequestsHaveFreshState(): void
    {
        $document = $this->createMock(Document::class);

        $response = $this->createMock(JsonApiResponse::class);
        $response->method('isSuccessfulIndex')->willReturn(true);
        $response->method('document')->willReturn($document);

        $hydrator = $this->createMock(Hydrator::class);
        $hydrator->method('hydrate')->willReturn(new Collection());

        $api = $this->createMock(Api::class);
        $api->expects($this->exactly(2))
            ->method('get')
            ->with('resources')
            ->willReturn($response);

        $client = new Client($api, $hydrator);

        $client->resources()->all();
        $client->resources()->all();
    }

    /**
     * Tests that one or more resources can be included using the client.
     */
    public function testClientIncludes(): void
    {
        $response = $this->createMock(JsonApiResponse::class);
        $response->method('isSuccessfulIndex')->willReturn(true);
        $response->method('document')->willReturn($this->createMock(Document::class));

        $hydrator = $this->createMock(Hydrator::class);
        $hydrator->method('hydrate')->willReturn(new Collection());

        $api = $this->createMock(Api::class);
        $api->expects($this->once())
            ->method('get')
            ->with('resources?include=relation1%2Crelation2')
            ->willReturn($response);

        $client = new Client($api, $hydrator);

        $client->resources()->include('relation1', 'relation2')->all();
    }
}
